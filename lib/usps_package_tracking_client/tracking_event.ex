defmodule UspsPackageTrackingClient.TrackingEvent do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:authorized_agent, :boolean)
    field(:delivery_attribute_code, :string)
    field(:event, :string)
    field(:event_city, :string)
    field(:event_code, :string)
    field(:event_country, :string)
    field(:event_date, UspsPackageTrackingClient.UspsDate)
    field(:event_state, :string)
    field(:event_time, UspsPackageTrackingClient.UspsTime)
    field(:event_zip_code, :string)
    field(:firm_name, :string)
    field(:name, :string)
  end

  @type t() :: %UspsPackageTrackingClient.TrackingEvent{
          authorized_agent: boolean(),
          delivery_attribute_code: String.t(),
          event: String.t(),
          event_city: String.t(),
          event_code: String.t(),
          event_country: String.t(),
          event_date: Date.t(),
          event_state: String.t(),
          event_time: Time.t(),
          event_zip_code: String.t(),
          firm_name: String.t(),
          name: String.t()
        }

  def changeset(tracking_info, attrs \\ %{}) do
    attrs =
      attrs
      |> UspsPackageTrackingClient.Map.rename_key("AuthorizedAgent", "authorized_agent")
      |> UspsPackageTrackingClient.Map.rename_key(
        "DeliveryAttributeCode",
        "delivery_attribute_code"
      )
      |> UspsPackageTrackingClient.Map.rename_key("Event", "event")
      |> UspsPackageTrackingClient.Map.rename_key("EventCity", "event_city")
      |> UspsPackageTrackingClient.Map.rename_key("EventCode", "event_code")
      |> UspsPackageTrackingClient.Map.rename_key("EventCountry", "event_country")
      |> UspsPackageTrackingClient.Map.rename_key("EventDate", "event_date")
      |> UspsPackageTrackingClient.Map.rename_key("EventState", "event_state")
      |> UspsPackageTrackingClient.Map.rename_key("EventTime", "event_time")
      |> UspsPackageTrackingClient.Map.rename_key("EventZIPCode", "event_zip_code")
      |> UspsPackageTrackingClient.Map.rename_key("FirmName", "firm_name")
      |> UspsPackageTrackingClient.Map.rename_key("Name", "name")

    tracking_info
    |> cast(attrs, [
      :authorized_agent,
      :delivery_attribute_code,
      :event,
      :event_city,
      :event_code,
      :event_country,
      :event_date,
      :event_state,
      :event_time,
      :event_zip_code,
      :firm_name,
      :name
    ])
  end
end
