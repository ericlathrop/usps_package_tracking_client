defmodule UspsPackageTrackingClient.TrackingResponse do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    embeds_many(:tracking_info, UspsPackageTrackingClient.TrackingInfo)
  end

  @type t() :: %UspsPackageTrackingClient.TrackingResponse{
          tracking_info: [UspsPackageTrackingClient.TrackingInfo.t()]
        }

  def changeset(tracking_response, attrs \\ %{}) do
    attrs =
      attrs
      |> UspsPackageTrackingClient.Map.rename_key("TrackInfo", "tracking_info")

    tracking_response
    |> cast(attrs, [])
    |> cast_embed(:tracking_info)
    |> validate_required([:tracking_info])
  end

  def build(attrs) do
    struct(UspsPackageTrackingClient.TrackingResponse)
    |> changeset(attrs)
    |> apply_action(:build)
  end
end
