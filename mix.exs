defmodule UspsPackageTrackingClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :usps_package_tracking_client,
      config_path: "config/config.exs",
      deps: deps(),
      description: "Client for the USPS Package Tracking API.",
      docs: docs(),
      elixir: "~> 1.10",
      homepage_url: "https://gitlab.com/ericlathrop/usps_package_tracking_client",
      name: "UspsPackageTrackingClient",
      package: package(),
      source_url: "https://gitlab.com/ericlathrop/usps_package_tracking_client",
      start_permanent: Mix.env() == :prod,
      version: "0.1.0"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ecto, "~> 3.4"},
      {:ex_doc, "~> 0.22", only: :dev, runtime: false},
      {:erlsom, "~> 1.5"},
      {:httpoison, "~> 1.7"},
      {:mix_test_watch, "~> 1.0", only: :dev, runtime: false}
    ]
  end

  defp docs do
    [
      main: "readme",
      extras: ["CHANGELOG.md", "README.md"]
    ]
  end

  defp package do
    [
      licenses: ["MIT"],
      links: %{
        "GitLab" => "https://gitlab.com/ericlathrop/usps_package_tracking_client"
      }
    ]
  end
end
