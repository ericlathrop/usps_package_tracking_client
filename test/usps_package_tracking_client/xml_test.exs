defmodule UspsPackageTrackingClient.XmlTest do
  use ExUnit.Case
  alias UspsPackageTrackingClient.Xml

  describe "xml_to_map/1" do
    test "with valid xml returns valid map" do
      {:ok, text} = File.read("test/data/successful_response.xml")
      {:ok, xml, _remainder} = :erlsom.simple_form(text)

      assert %{
               "TrackResponse" => %{
                 "TrackInfo" => [
                   %{
                     "Class" => "Media Mail",
                     "ClassOfMailCode" => "BS",
                     "DestinationCity" => "LOUISVILLE",
                     "DestinationState" => "KY",
                     "DestinationZip" => "40299",
                     "EmailEnabled" => "true",
                     "ExpectedDeliveryDate" => "September 11, 2020",
                     "ID" => "9449011202555982346894",
                     "KahalaIndicator" => "false",
                     "MPDATE" => "2020-09-03 14:08:41.000000",
                     "MPSUFFIX" => "745607323",
                     "MailTypeCode" => "DM",
                     "OnTime" => "false",
                     "OriginCity" => "FRUITLAND",
                     "OriginState" => "ID",
                     "OriginZip" => "83619",
                     "PodEnabled" => "false",
                     "RestoreEnabled" => "false",
                     "RramEnabled" => "false",
                     "RreEnabled" => "false",
                     "Service" => "USPS Tracking<SUP>&#174;</SUP>",
                     "ServiceTypeCode" => "490",
                     "Status" => "In Transit, Arriving On Time",
                     "StatusCategory" => "In Transit",
                     "StatusSummary" =>
                       "Your package is moving within the USPS network and is on track to be delivered the expected delivery date. It is currently in transit to the next facility.",
                     "TABLECODE" => "T",
                     "TPodEnabled" => "false",
                     "TrackDetail" => [
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Departed USPS Regional Facility",
                         "EventCity" => "DES MOINES IA NETWORK DISTRIBUTION CENTER",
                         "EventCode" => "T1",
                         "EventCountry" => nil,
                         "EventDate" => "September 8,\n2020",
                         "EventState" => nil,
                         "EventTime" => "5:33 pm",
                         "EventZIPCode" => nil,
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Arrived at USPS Regional Facility",
                         "EventCity" => "DES MOINES IA NETWORK DISTRIBUTION CENTER",
                         "EventCode" => "U1",
                         "EventCountry" => nil,
                         "EventDate" => "September 8, 2020",
                         "EventState" => nil,
                         "EventTime" => "11:39 am",
                         "EventZIPCode" => nil,
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Departed USPS Regional Facility",
                         "EventCity" => "DENVER CO NETWORK DISTRIBUTION CENTER",
                         "EventCode" => "L1",
                         "EventCountry" => nil,
                         "EventDate" => "September 7, 2020",
                         "EventState" => nil,
                         "EventTime" => "1:15 pm",
                         "EventZIPCode" => nil,
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Arrived at USPS Regional Facility",
                         "EventCity" => "DENVER CO NETWORK DISTRIBUTION CENTER",
                         "EventCode" => "U1",
                         "EventCountry" => nil,
                         "EventDate" => "September 7, 2020",
                         "EventState" => nil,
                         "EventTime" => "5:07 am",
                         "EventZIPCode" => nil,
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Arrived at USPS Regional Facility",
                         "EventCity" => "DENVER CO  DISTRIBUTION CENTER",
                         "EventCode" => "A1",
                         "EventCountry" => nil,
                         "EventDate" => "September 6, 2020",
                         "EventState" => nil,
                         "EventTime" => "6:40 am",
                         "EventZIPCode" => nil,
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Departed USPS Facility",
                         "EventCity" => "WEST VALLEY CITY",
                         "EventCode" => "T1",
                         "EventCountry" => nil,
                         "EventDate" => "September 5, 2020",
                         "EventState" => "UT",
                         "EventTime" => "8:43 pm",
                         "EventZIPCode" => "84119",
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Arrived at USPS Facility",
                         "EventCity" => "WEST VALLEY CITY",
                         "EventCode" => "A1",
                         "EventCountry" => nil,
                         "EventDate" => "September 5, 2020",
                         "EventState" => "UT",
                         "EventTime" => "11:49 am",
                         "EventZIPCode" => "84119",
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Departed USPS Regional Facility",
                         "EventCity" => "BOISE ID DISTRIBUTION CENTER",
                         "EventCode" => "T1",
                         "EventCountry" => nil,
                         "EventDate" => "September 5, 2020",
                         "EventState" => nil,
                         "EventTime" => "3:38 am",
                         "EventZIPCode" => nil,
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Arrived at USPS Regional Origin Facility",
                         "EventCity" => "BOISE ID DISTRIBUTION CENTER",
                         "EventCode" => "10",
                         "EventCountry" => nil,
                         "EventDate" => "September 4, 2020",
                         "EventState" => nil,
                         "EventTime" => "9:56 pm",
                         "EventZIPCode" => nil,
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "Departed Post Office",
                         "EventCity" => "FRUITLAND",
                         "EventCode" => "SF",
                         "EventCountry" => nil,
                         "EventDate" => "September 4, 2020",
                         "EventState" => "ID",
                         "EventTime" => "5:11 pm",
                         "EventZIPCode" => "83619",
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "USPS in possession of item",
                         "EventCity" => "FRUITLAND",
                         "EventCode" => "03",
                         "EventCountry" => nil,
                         "EventDate" => "September 4, 2020",
                         "EventState" => "ID",
                         "EventTime" => "1:41 pm",
                         "EventZIPCode" => "83619",
                         "FirmName" => nil,
                         "Name" => nil
                       },
                       %{
                         "AuthorizedAgent" => "false",
                         "DeliveryAttributeCode" => "33",
                         "Event" => "Shipping Label Created, USPS Awaiting Item",
                         "EventCity" => "FRUITLAND",
                         "EventCode" => "GX",
                         "EventCountry" => nil,
                         "EventDate" => "September 3, 2020",
                         "EventState" => "ID",
                         "EventTime" => "4:22 pm",
                         "EventZIPCode" => "83619",
                         "FirmName" => nil,
                         "Name" => nil
                       }
                     ],
                     "TrackSummary" => [
                       %{
                         "AuthorizedAgent" => "false",
                         "Event" => "In Transit, Arriving On Time",
                         "EventCity" => nil,
                         "EventCode" => "NT",
                         "EventCountry" => nil,
                         "EventDate" => "September 9, 2020",
                         "EventState" => nil,
                         "EventTime" => nil,
                         "EventZIPCode" => nil,
                         "FirmName" => nil,
                         "Name" => nil
                       }
                     ]
                   }
                 ]
               }
             } ==
               Xml.xml_to_map(xml)
    end

    test "with single child text returns one pair map" do
      assert %{"tag" => "text"} == Xml.xml_to_map({'tag', [], ['text']})
    end

    test "with nested elements returns nested maps" do
      assert %{"a" => %{"b" => "c"}} == Xml.xml_to_map({'a', [], [{'b', [], ['c']}]})
    end

    test "with empty element returns nil in map" do
      assert %{"a" => %{"b" => nil}} == Xml.xml_to_map({'a', [], [{'b', [], []}]})
    end

    test "with attribute returns maps with attribute in value" do
      assert %{"a" => %{"b" => "c", "d" => "e"}} ==
               Xml.xml_to_map({'a', [{'d', 'e'}], [{'b', [], ['c']}]})
    end

    test "with grouped elements returns arrays in maps" do
      assert %{"a" => %{"TrackDetail" => [%{"b" => "c"}]}} ==
               Xml.xml_to_map({'a', [], [{'TrackDetail', [], [{'b', [], ['c']}]}]})
    end
  end
end
