defmodule UspsPackageTrackingClient.Error do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:description, :string)
    field(:help_context, :string)
    field(:help_file, :string)
    field(:number, :string)
    field(:source, :string)
  end

  @type t() :: %UspsPackageTrackingClient.Error{
          description: String.t(),
          help_context: String.t(),
          help_file: String.t(),
          number: String.t(),
          source: String.t()
        }

  def changeset(tracking_info, attrs \\ %{}) do
    attrs =
      attrs
      |> UspsPackageTrackingClient.Map.rename_key("Description", "description")
      |> UspsPackageTrackingClient.Map.rename_key("HelpContext", "help_context")
      |> UspsPackageTrackingClient.Map.rename_key("HelpFile", "help_file")
      |> UspsPackageTrackingClient.Map.rename_key("Number", "number")
      |> UspsPackageTrackingClient.Map.rename_key("Source", "source")

    tracking_info
    |> cast(attrs, [
      :description,
      :help_context,
      :help_file,
      :number,
      :source
    ])
  end

  def build(attrs) do
    struct(UspsPackageTrackingClient.Error)
    |> changeset(attrs)
    |> apply_action(:build)
  end
end
