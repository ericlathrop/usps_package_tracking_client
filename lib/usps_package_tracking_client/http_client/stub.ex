defmodule UspsPackageTrackingClient.HttpClient.Stub do
  @behaviour UspsPackageTrackingClient.HttpClient

  @impl UspsPackageTrackingClient.HttpClient
  def get(url, _headers, _options) do
    %URI{query: query} = URI.parse(url)
    %{"XML" => xml} = URI.decode_query(query)
    {:ok, xml, _remainder} = :erlsom.simple_form(xml)

    {'TrackFieldRequest', [{'USERID', user_name}],
     [
       {'Revision', [], ['1']},
       {'ClientIp', [], client_ip},
       {'SourceId', [], source_id},
       {'TrackID', [{'ID', tracking_number}], []}
     ]} = xml

    fake_data(
      to_string(tracking_number),
      to_string(user_name),
      to_string(client_ip),
      to_string(source_id)
    )
  end

  defp fake_data("9449011202555982346894", "invalid", "client_ip", "source_id") do
    fake_response(200, "invalid_username_error")
  end

  defp fake_data("unknown", "user_name", "client_ip", "source_id") do
    fake_response(200, "unknown_tracking_number_error")
  end

  defp fake_data("", "user_name", "client_ip", "source_id") do
    fake_response(200, "missing_tracking_number_error")
  end

  defp fake_data("9449011202555982346894", "", "client_ip", "source_id") do
    fake_response(200, "missing_user_name_error")
  end

  defp fake_data("9449011202555982346894", "user_name", "", "source_id") do
    fake_response(200, "missing_client_ip_error")
  end

  defp fake_data("9449011202555982346894", "user_name", "client_ip", "") do
    fake_response(200, "missing_source_id_error")
  end

  defp fake_data("9449011202555982346894", "user_name", "client_ip", "source_id") do
    fake_response(200, "successful_response")
  end

  defp fake_data(_tracking_number, _user_name, _client_id, _source_id) do
    {:error, "can't find fake data"}
  end

  defp fake_response(status_code, data_file) do
    {:ok, xml} = File.read("test/data/#{data_file}.xml")
    {:ok, %HTTPoison.Response{status_code: status_code, body: xml}}
  end
end
