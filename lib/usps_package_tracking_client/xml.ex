defmodule UspsPackageTrackingClient.Xml do
  @moduledoc false

  def xml_to_map(xml) do
    [xml_to_tuple(xml)]
    |> Map.new()
  end

  defp xml_to_tuple({element, [], []}) do
    {to_string(element), nil}
  end

  defp xml_to_tuple(single_value) when is_list(single_value) do
    {"_internal_text", to_string(single_value)}
  end

  defp xml_to_tuple({element, attrs, children}) do
    children_map =
      children
      |> Enum.map(&xml_to_tuple(&1))
      |> Enum.group_by(fn {k, _v} -> k end, fn {_k, v} -> v end)
      |> Enum.map(fn {key, value} ->
        cond do
          is_grouped_key(key) ->
            {key, value}

          true ->
            [last_value] = value
            {key, last_value}
        end
      end)
      |> Map.new()

    params =
      attrs
      |> Enum.map(fn {key, value} when is_list(key) and is_list(value) ->
        {to_string(key), to_string(value)}
      end)
      |> Map.new()
      |> Map.merge(children_map)

    {to_string(element), params}
    |> collapse_single_values()
  end

  defp collapse_single_values({key, %{"_internal_text" => value}}) do
    {key, value}
  end

  # defp collapse_single_values({key, }) do
  #   {key, nil}
  # end

  defp collapse_single_values(any), do: any

  defp is_grouped_key("Error"), do: true
  defp is_grouped_key("TrackDetail"), do: true
  defp is_grouped_key("TrackInfo"), do: true
  defp is_grouped_key("TrackSummary"), do: true
  defp is_grouped_key(_), do: false
end
