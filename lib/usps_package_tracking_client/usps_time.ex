defmodule UspsPackageTrackingClient.UspsTime do
  use Ecto.Type
  def type, do: :time

  def cast(time) when is_binary(time) do
    String.split(time, ~r/[ :]/)
    |> parse_time()
  end

  def cast(_), do: :error

  defp parse_time([hour_string, minute_string, ampm]) do
    with {twelve_hour, _} <- Integer.parse(hour_string),
         twenty_four_hour <- twelve_hour_to_twenty_four_hour(twelve_hour, ampm),
         {minute, _} <- Integer.parse(minute_string) do
      Time.new(twenty_four_hour, minute, 0)
    else
      _ -> :error
    end
  end

  defp parse_time(_), do: :error

  defp twelve_hour_to_twenty_four_hour(12, "am"), do: 0
  defp twelve_hour_to_twenty_four_hour(hour, "am"), do: hour
  defp twelve_hour_to_twenty_four_hour(12, "pm"), do: 12
  defp twelve_hour_to_twenty_four_hour(hour, "pm"), do: hour + 12

  def load(erl_time = {_hour, _minute, _second}), do: {:ok, Time.from_erl(erl_time)}

  def dump(time = %Time{}), do: {:ok, Time.to_erl(time)}
  def dump(_), do: :error
end
