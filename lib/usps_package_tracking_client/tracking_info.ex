defmodule UspsPackageTrackingClient.TrackingInfo do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:id, :string)
    field(:class, :string)
    field(:class_of_mail_code, :string)
    field(:destination_city, :string)
    field(:destination_state, :string)
    field(:destination_zip, :string)
    field(:email_enabled, :boolean)
    field(:expected_delivery_date, UspsPackageTrackingClient.UspsDate)
    field(:kahala_indicator, :boolean)
    field(:on_time, :boolean)
    field(:origin_city, :string)
    field(:origin_state, :string)
    field(:origin_zip, :string)
    field(:mp_date, :naive_datetime)
    field(:mp_suffix, :string)
    field(:mail_type_code, :string)
    field(:pod_enabled, :boolean)
    field(:restore_enabled, :boolean)
    field(:rram_enabled, :boolean)
    field(:rre_enabled, :boolean)
    field(:service, :string)
    field(:service_type_code, :string)
    field(:status, :string)
    field(:status_category, :string)
    field(:status_summary, :string)
    field(:table_code, :string)
    field(:tpod_enabled, :boolean)
    embeds_many(:tracking_detail, UspsPackageTrackingClient.TrackingEvent)
    embeds_many(:tracking_summary, UspsPackageTrackingClient.TrackingEvent)
    embeds_many(:errors, UspsPackageTrackingClient.Error)
  end

  @type t() :: %UspsPackageTrackingClient.TrackingInfo{
          id: String.t(),
          class: String.t(),
          class_of_mail_code: String.t(),
          destination_city: String.t(),
          destination_state: String.t(),
          destination_zip: String.t(),
          email_enabled: boolean(),
          expected_delivery_date: Date.t(),
          kahala_indicator: boolean(),
          on_time: boolean(),
          origin_city: String.t(),
          origin_state: String.t(),
          origin_zip: String.t(),
          mp_date: NaiveDateTime.t(),
          mp_suffix: String.t(),
          mail_type_code: String.t(),
          pod_enabled: boolean(),
          restore_enabled: boolean(),
          rram_enabled: boolean(),
          rre_enabled: boolean(),
          service: String.t(),
          service_type_code: String.t(),
          status: String.t(),
          status_category: String.t(),
          status_summary: String.t(),
          table_code: String.t(),
          tpod_enabled: boolean(),
          tracking_detail: [UspsPackageTrackingClient.TrackingEvent.t()],
          tracking_summary: [UspsPackageTrackingClient.TrackingEvent.t()],
          errors: [UspsPackageTrackingClient.Error.t()]
        }

  def changeset(tracking_info, attrs \\ %{}) do
    attrs =
      attrs
      |> UspsPackageTrackingClient.Map.rename_key("ID", "id")
      |> UspsPackageTrackingClient.Map.rename_key("Class", "class")
      |> UspsPackageTrackingClient.Map.rename_key("ClassOfMailCode", "class_of_mail_code")
      |> UspsPackageTrackingClient.Map.rename_key("DestinationCity", "destination_city")
      |> UspsPackageTrackingClient.Map.rename_key("DestinationState", "destination_state")
      |> UspsPackageTrackingClient.Map.rename_key("DestinationZip", "destination_zip")
      |> UspsPackageTrackingClient.Map.rename_key("EmailEnabled", "email_enabled")
      |> UspsPackageTrackingClient.Map.rename_key(
        "ExpectedDeliveryDate",
        "expected_delivery_date"
      )
      |> UspsPackageTrackingClient.Map.rename_key("KahalaIndicator", "kahala_indicator")
      |> UspsPackageTrackingClient.Map.rename_key("MPDATE", "mp_date")
      |> UspsPackageTrackingClient.Map.rename_key("MPSUFFIX", "mp_suffix")
      |> UspsPackageTrackingClient.Map.rename_key("MailTypeCode", "mail_type_code")
      |> UspsPackageTrackingClient.Map.rename_key("OnTime", "on_time")
      |> UspsPackageTrackingClient.Map.rename_key("OriginCity", "origin_city")
      |> UspsPackageTrackingClient.Map.rename_key("OriginState", "origin_state")
      |> UspsPackageTrackingClient.Map.rename_key("OriginZip", "origin_zip")
      |> UspsPackageTrackingClient.Map.rename_key("PodEnabled", "pod_enabled")
      |> UspsPackageTrackingClient.Map.rename_key("RestoreEnabled", "restore_enabled")
      |> UspsPackageTrackingClient.Map.rename_key("RramEnabled", "rram_enabled")
      |> UspsPackageTrackingClient.Map.rename_key("RreEnabled", "rre_enabled")
      |> UspsPackageTrackingClient.Map.rename_key("Service", "service")
      |> UspsPackageTrackingClient.Map.rename_key("ServiceTypeCode", "service_type_code")
      |> UspsPackageTrackingClient.Map.rename_key("Status", "status")
      |> UspsPackageTrackingClient.Map.rename_key("StatusCategory", "status_category")
      |> UspsPackageTrackingClient.Map.rename_key("StatusSummary", "status_summary")
      |> UspsPackageTrackingClient.Map.rename_key("TABLECODE", "table_code")
      |> UspsPackageTrackingClient.Map.rename_key("TPodEnabled", "tpod_enabled")
      |> UspsPackageTrackingClient.Map.rename_key("TrackDetail", "tracking_detail")
      |> UspsPackageTrackingClient.Map.rename_key("TrackSummary", "tracking_summary")
      |> UspsPackageTrackingClient.Map.rename_key("Error", "errors")

    tracking_info
    |> cast(attrs, [
      :id,
      :class,
      :class_of_mail_code,
      :destination_city,
      :destination_state,
      :destination_zip,
      :email_enabled,
      :expected_delivery_date,
      :kahala_indicator,
      :mp_date,
      :mp_suffix,
      :mail_type_code,
      :on_time,
      :origin_city,
      :origin_state,
      :origin_zip,
      :pod_enabled,
      :restore_enabled,
      :rram_enabled,
      :rre_enabled,
      :service,
      :service_type_code,
      :status,
      :status_category,
      :status_summary,
      :table_code,
      :tpod_enabled
    ])
    |> cast_embed(:tracking_detail)
    |> cast_embed(:tracking_summary)
    |> cast_embed(:errors)
  end
end
