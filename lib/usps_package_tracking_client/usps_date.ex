defmodule UspsPackageTrackingClient.UspsDate do
  use Ecto.Type
  def type, do: :date

  def cast(time) when is_binary(time) do
    String.split(time, ~r/[\s,]+/)
    |> parse_date()
  end

  def cast(_), do: :error

  defp parse_date([month_name, day, year]) do
    with month <- month_name_to_number(month_name),
         {day, _} <- Integer.parse(day),
         {year, _} <- Integer.parse(year) do
      Date.new(year, month, day)
    else
      _ -> :error
    end
  end

  defp month_name_to_number("January"), do: 1
  defp month_name_to_number("February"), do: 2
  defp month_name_to_number("March"), do: 3
  defp month_name_to_number("April"), do: 4
  defp month_name_to_number("May"), do: 5
  defp month_name_to_number("June"), do: 6
  defp month_name_to_number("July"), do: 7
  defp month_name_to_number("August"), do: 8
  defp month_name_to_number("September"), do: 9
  defp month_name_to_number("October"), do: 10
  defp month_name_to_number("November"), do: 11
  defp month_name_to_number("December"), do: 12

  def load(erl_date = {_year, _month, _day}), do: {:ok, Date.from_erl(erl_date)}

  def dump(date = %Date{}), do: {:ok, Date.to_erl(date)}
  def dump(_), do: :error
end
