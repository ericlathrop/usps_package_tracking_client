# UspsPackageTrackingClient

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `usps_package_tracking_client` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:usps_package_tracking_client, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/usps_package_tracking_client](https://hexdocs.pm/usps_package_tracking_client).

