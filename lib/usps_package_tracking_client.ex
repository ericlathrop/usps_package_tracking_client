defmodule UspsPackageTrackingClient do
  alias UspsPackageTrackingClient.Xml

  @http_client Application.get_env(
                 :usps_package_tracking_client,
                 :http_client,
                 HTTPoison
               )

  @spec track(String.t(), String.t(), String.t(), String.t()) ::
          {:ok, UspsPackageTrackingClient.TrackingResponse.t()} | {:error, any()}
  def track(tracking_number, user_name, client_ip \\ "127.0.0.1", source_id \\ "20260") do
    url =
      track_request_xml(user_name, client_ip, source_id, tracking_number)
      |> track_request_uri

    with {:ok, %HTTPoison.Response{status_code: 200, body: body}} <-
           @http_client.get(url, [], []),
         {:ok, xml, _remainder} <- :erlsom.simple_form(body) do
      handle_response(xml)
    else
      err -> err
    end
  end

  defp handle_response({'Error', _attrs, _reason} = xml) do
    %{"Error" => attrs} = Xml.xml_to_map(xml)

    case UspsPackageTrackingClient.Error.build(attrs) do
      {:ok, error} -> {:error, error}
      error -> error
    end
  end

  defp handle_response({'TrackResponse', [], _children} = xml) do
    %{"TrackResponse" => attrs} = Xml.xml_to_map(xml)
    UspsPackageTrackingClient.TrackingResponse.build(attrs)
  end

  defp track_request_uri(xml) do
    query =
      %{
        "API" => "TrackV2",
        "XML" => xml
      }
      |> URI.encode_query()

    %URI{
      scheme: "https",
      host: "secure.shippingapis.com",
      path: "/ShippingAPI.dll",
      query: query
    }
  end

  defp track_request_xml(user_name, client_ip, source_id, tracking_number) do
    """
    <TrackFieldRequest USERID="#{user_name}">
      <Revision>1</Revision>
      <ClientIp>#{client_ip}</ClientIp>
      <SourceId>#{source_id}</SourceId>
      <TrackID ID="#{tracking_number}" />
    </TrackFieldRequest>
    """
  end
end
