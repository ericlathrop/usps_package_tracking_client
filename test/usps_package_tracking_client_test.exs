defmodule UspsPackageTrackingClientTest do
  use ExUnit.Case
  doctest UspsPackageTrackingClient

  describe "track/4" do
    test "with missing tracking_number returns error" do
      assert {:error,
              %UspsPackageTrackingClient.Error{
                description:
                  "The 'ID' attribute is invalid - The value '' is invalid according to its datatype 'http://www.w3.org/2001/XMLSchema:NMTOKEN' - The empty string '' is not a valid name.",
                number: "-2147221301",
                source: "clsTrack:getTrackInfo2"
              }} ==
               UspsPackageTrackingClient.track(
                 "",
                 "user_name",
                 "client_ip",
                 "source_id"
               )
    end

    test "with unknown tracking_number returns error" do
      assert {:ok,
              %UspsPackageTrackingClient.TrackingResponse{
                tracking_info: [
                  %UspsPackageTrackingClient.TrackingInfo{
                    errors: [
                      %UspsPackageTrackingClient.Error{
                        description:
                          "The tracking number may be incorrect or the status update is not yet available. Please verify your tracking number and try again later.",
                        number: "-2147219302"
                      }
                    ],
                    id: "123"
                  }
                ]
              }} ==
               UspsPackageTrackingClient.track(
                 "unknown",
                 "user_name",
                 "client_ip",
                 "source_id"
               )
    end

    test "with missing user_name returns error" do
      assert {:error,
              %UspsPackageTrackingClient.Error{
                description:
                  "XML Syntax Error: Please check the XML request to see if it can be parsed.(B)",
                number: "80040B19",
                source: "USPSCOM::DoAuth"
              }} ==
               UspsPackageTrackingClient.track(
                 "9449011202555982346894",
                 "",
                 "client_ip",
                 "source_id"
               )
    end

    test "with invalid user_name returns error" do
      assert {:error,
              %UspsPackageTrackingClient.Error{
                description:
                  "Authorization failure.  Perhaps username and/or password is incorrect.",
                number: "80040B1A",
                source: "USPSCOM::DoAuth"
              }} ==
               UspsPackageTrackingClient.track(
                 "9449011202555982346894",
                 "invalid",
                 "client_ip",
                 "source_id"
               )
    end

    test "with missing client_ip returns error" do
      assert {:error,
              %UspsPackageTrackingClient.Error{
                description: "ClientIp tag required",
                number: "-2147219289",
                source: "clsTrack:getTrackInfo2"
              }} ==
               UspsPackageTrackingClient.track(
                 "9449011202555982346894",
                 "user_name",
                 "",
                 "source_id"
               )
    end

    test "with missing source_id returns error" do
      assert {:error,
              %UspsPackageTrackingClient.Error{
                description: "SourceId tag required",
                number: "-2147219288",
                source: "clsTrack:getTrackInfo2"
              }} ==
               UspsPackageTrackingClient.track(
                 "9449011202555982346894",
                 "user_name",
                 "client_ip",
                 ""
               )
    end

    test "with a valid tracking number and username returns parsed response" do
      assert {:ok,
              %UspsPackageTrackingClient.TrackingResponse{
                tracking_info: [
                  %UspsPackageTrackingClient.TrackingInfo{
                    class: "Media Mail",
                    class_of_mail_code: "BS",
                    destination_city: "LOUISVILLE",
                    destination_state: "KY",
                    destination_zip: "40299",
                    email_enabled: true,
                    expected_delivery_date: ~D[2020-09-11],
                    id: "9449011202555982346894",
                    kahala_indicator: false,
                    mail_type_code: "DM",
                    mp_date: ~N[2020-09-03 14:08:41],
                    mp_suffix: "745607323",
                    on_time: false,
                    origin_city: "FRUITLAND",
                    origin_state: "ID",
                    origin_zip: "83619",
                    pod_enabled: false,
                    restore_enabled: false,
                    rram_enabled: false,
                    rre_enabled: false,
                    service: "USPS Tracking<SUP>&#174;</SUP>",
                    service_type_code: "490",
                    status: "In Transit, Arriving On Time",
                    status_category: "In Transit",
                    status_summary:
                      "Your package is moving within the USPS network and is on track to be delivered the expected delivery date. It is currently in transit to the next facility.",
                    table_code: "T",
                    tpod_enabled: false,
                    tracking_detail: [
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Departed USPS Regional Facility",
                        event_city: "DES MOINES IA NETWORK DISTRIBUTION CENTER",
                        event_code: "T1",
                        event_country: nil,
                        event_date: ~D[2020-09-08],
                        event_state: nil,
                        event_time: ~T[17:33:00],
                        event_zip_code: nil,
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Arrived at USPS Regional Facility",
                        event_city: "DES MOINES IA NETWORK DISTRIBUTION CENTER",
                        event_code: "U1",
                        event_country: nil,
                        event_date: ~D[2020-09-08],
                        event_state: nil,
                        event_time: ~T[11:39:00],
                        event_zip_code: nil,
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Departed USPS Regional Facility",
                        event_city: "DENVER CO NETWORK DISTRIBUTION CENTER",
                        event_code: "L1",
                        event_country: nil,
                        event_date: ~D[2020-09-07],
                        event_state: nil,
                        event_time: ~T[13:15:00],
                        event_zip_code: nil,
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Arrived at USPS Regional Facility",
                        event_city: "DENVER CO NETWORK DISTRIBUTION CENTER",
                        event_code: "U1",
                        event_country: nil,
                        event_date: ~D[2020-09-07],
                        event_state: nil,
                        event_time: ~T[05:07:00],
                        event_zip_code: nil,
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Arrived at USPS Regional Facility",
                        event_city: "DENVER CO  DISTRIBUTION CENTER",
                        event_code: "A1",
                        event_country: nil,
                        event_date: ~D[2020-09-06],
                        event_state: nil,
                        event_time: ~T[06:40:00],
                        event_zip_code: nil,
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Departed USPS Facility",
                        event_city: "WEST VALLEY CITY",
                        event_code: "T1",
                        event_country: nil,
                        event_date: ~D[2020-09-05],
                        event_state: "UT",
                        event_time: ~T[20:43:00],
                        event_zip_code: "84119",
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Arrived at USPS Facility",
                        event_city: "WEST VALLEY CITY",
                        event_code: "A1",
                        event_country: nil,
                        event_date: ~D[2020-09-05],
                        event_state: "UT",
                        event_time: ~T[11:49:00],
                        event_zip_code: "84119",
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Departed USPS Regional Facility",
                        event_city: "BOISE ID DISTRIBUTION CENTER",
                        event_code: "T1",
                        event_country: nil,
                        event_date: ~D[2020-09-05],
                        event_state: nil,
                        event_time: ~T[03:38:00],
                        event_zip_code: nil,
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Arrived at USPS Regional Origin Facility",
                        event_city: "BOISE ID DISTRIBUTION CENTER",
                        event_code: "10",
                        event_country: nil,
                        event_date: ~D[2020-09-04],
                        event_state: nil,
                        event_time: ~T[21:56:00],
                        event_zip_code: nil,
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "Departed Post Office",
                        event_city: "FRUITLAND",
                        event_code: "SF",
                        event_country: nil,
                        event_date: ~D[2020-09-04],
                        event_state: "ID",
                        event_time: ~T[17:11:00],
                        event_zip_code: "83619",
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "USPS in possession of item",
                        event_city: "FRUITLAND",
                        event_code: "03",
                        event_country: nil,
                        event_date: ~D[2020-09-04],
                        event_state: "ID",
                        event_time: ~T[13:41:00],
                        event_zip_code: "83619",
                        firm_name: nil,
                        name: nil
                      },
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: "33",
                        event: "Shipping Label Created, USPS Awaiting Item",
                        event_city: "FRUITLAND",
                        event_code: "GX",
                        event_country: nil,
                        event_date: ~D[2020-09-03],
                        event_state: "ID",
                        event_time: ~T[16:22:00],
                        event_zip_code: "83619",
                        firm_name: nil,
                        name: nil
                      }
                    ],
                    tracking_summary: [
                      %UspsPackageTrackingClient.TrackingEvent{
                        authorized_agent: false,
                        delivery_attribute_code: nil,
                        event: "In Transit, Arriving On Time",
                        event_city: nil,
                        event_code: "NT",
                        event_country: nil,
                        event_date: ~D[2020-09-09],
                        event_state: nil,
                        event_time: nil,
                        event_zip_code: nil,
                        firm_name: nil,
                        name: nil
                      }
                    ]
                  }
                ]
              }} ==
               UspsPackageTrackingClient.track(
                 "9449011202555982346894",
                 "user_name",
                 "client_ip",
                 "source_id"
               )
    end
  end
end
