use Mix.Config

config :usps_package_tracking_client,
  http_client: UspsPackageTrackingClient.HttpClient.Stub
